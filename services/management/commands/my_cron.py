from django.core.management.base import BaseCommand
from django.conf import settings
from services.cron.cron_runner import CronRunner


class Command(BaseCommand):
    help = 'My Cron'

    def handle(self, *args, **options):
        runner = CronRunner(settings.BASE_DIR)
        if options['run']:
            runner.run()
        elif options['list']:
            runner.list()

    def add_arguments(self, parser):
        parser.add_argument(
            '-r',
            '--run',
            action='store_true',
            default=False,
            help='Scan cron-tasks and run them'
        )
        parser.add_argument(
            '-l',
            '--list',
            action='store_true',
            default=False,
            help='Scan cron-tasks and print list'
        )