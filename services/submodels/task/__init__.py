from django.db import models
from django.utils import timezone
import datetime
from typing import List
import json
from ..consumer.model import Consumer
from .scheduller_model import Scheduller


class Task(models.Model):
    """
    Main object of task.
    !Attention! You must not create this objects in database with admin panel or Task.objects.create(...)
    Task creates only with cron service.
    You can to change this object after creating in admin panel or another ways...
    """
    # relations
    consumers = models.ManyToManyField(Consumer, blank=True)
    scheduller = models.OneToOneField(Scheduller, blank=True, on_delete=models.SET_NULL, null=True)
    # fields
    name = models.CharField(max_length=1000)
    run_every_mins = models.IntegerField(null=True)
    run_at_time = models.CharField(max_length=2000, null=True)
    max_minutes_in_process = models.IntegerField(default=180)
    running_now = models.BooleanField(default=False)
    alert = models.BooleanField(default=False)
    include = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    @property
    def last_run(self) -> datetime.datetime:
        history = self.taskevent_set.all().order_by("-started")
        if len(history) > 0:
            return history[0].started
        else:
            return datetime.datetime(year=1990, month=1, day=1, tzinfo=timezone.utc)

    def dump_run_at_time(self, lst: List[datetime.time]) -> None:
        string_list = []
        for time in lst:
            string_list.append(time.strftime("%H:%M"))
        self.run_at_time = json.dumps(string_list)

    def load_run_at_time(self) -> List[datetime.time] or list:
        string_list = json.loads(self.run_at_time)
        times_list = []
        for time in string_list:
            times_list.append(datetime.datetime.strptime(time, "%H:%M"))
        return times_list

    def is_limit_exceeded(self) -> bool:
        now = timezone.now()
        delta = now - self.last_run
        delta_min = delta.total_seconds() // 60
        if delta_min > self.max_minutes_in_process and self.running_now:
            return True
        return False


class TaskEvent(models.Model):
    """
    Task Event.
    He is created for each action in main task.
    Contains log if was error...
    """
    # Relations
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    # Fields
    started = models.DateTimeField()
    finished = models.DateTimeField(null=True)
    success = models.BooleanField(default=True)
    log = models.TextField(default="", blank=False)

    def __str__(self):
        return self.started.strftime("%H:%M") + " / " + self.task.name
