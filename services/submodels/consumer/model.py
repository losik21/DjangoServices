from django.db import models
import requests


class Consumer(models.Model):
    name = models.CharField(max_length=1000, verbose_name="Consumer name")
    bot_token = models.CharField(max_length=1000, verbose_name="Telegram bot token", blank=True)
    telegram_chat_id = models.CharField(max_length=100, verbose_name="Telegram bot user chat_id", blank=True)
    email = models.CharField(max_length=1000, verbose_name="E-mail")
    telegram_included = models.BooleanField(default=False)
    email_included = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def send_text_message(self, message: str):
        if self.telegram_included:
            url = f'https://api.telegram.org/bot{self.bot_token}/sendMessage'
            params = {
                'chat_id': self.telegram_chat_id,
                'text': message,
            }
            response = requests.get(url=url, params=params)

        if self.email_included:
            ...

    def send_message_with_file(self, message: str, file):
        if self.telegram_included:
            url = f'https://api.telegram.org/bot{self.bot_token}/sendDocument'
            data = {
                'chat_id': self.telegram_chat_id,
                'caption': message,
            }
            files = {
                'document': file,
            }
            response = requests.post(url=url, data=data, files=files)

        if self.email_included:
            ...
