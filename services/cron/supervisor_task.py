from ..models import Task


def supervisor_task_function():
    all_tasks = Task.objects.all().exclude(name="SupervisorTask")
    for task in all_tasks:
        if task.is_limit_exceeded():
            task.running_now = False
            task.save()