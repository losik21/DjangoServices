from services.submodels.task import Task, TaskEvent
import traceback
import threading
from abc import ABC, abstractmethod
from django.utils import timezone


class CronTask(ABC):
    """
    CronTask.
    """
    RUN_EVERY_MINS = 0
    RUN_AT_TIME = []
    ALERT = False
    INCLUDE = True

    def __init__(self):
        self.db_task = self.registry()
        self.task_event: TaskEvent or None = None
        if self.check_time():
            self.start_task()
            self.new_thread()

    def __repr__(self):
        return self.__class__.name()

    @abstractmethod
    def do(self):
        """ Abstract function which must implemented in new task and must contains client code """
        pass

    def check_time(self):
        if not self.db_task.scheduller:
            is_in_schedulle = True
        else:
            is_in_schedulle = self.db_task.scheduller.is_time()
        if self.is_time and self.INCLUDE and self.db_task.running_now == False and is_in_schedulle:
            return True
        return False

    def run(self):
        print(f"Start Task {self.__class__.name}")
        try:
            self.do()
            self.finish_success_task()
            print(f"Finish success {self.__class__.name}")
        except Exception as e:
            short_error = e
            trace = traceback.format_exc()
            self.finish_error_task(short_error, trace)
            print(f"Finish Error{self.__class__.name}")

    @classmethod
    def name(cls):
        return cls.__name__

    def new_thread(self):
        new_thread = threading.Thread(target=self.run, args=())
        new_thread.start()

    def registry(self) -> Task or None:
        task_filter = Task.objects.filter(name=self.__class__.__name__)
        if len(task_filter) == 0:
            try:
                task = Task()
                task.name = self.__class__.__name__
                task.run_every_mins = self.RUN_EVERY_MINS
                task.alert = self.ALERT
                task.dump_run_at_time(self.RUN_AT_TIME)
                task.save()
                return task
            except:
                return None
        else:
            try:
                task = task_filter[0]
                self.RUN_EVERY_MINS = task.run_every_mins
                self.ALERT = task.alert
                self.INCLUDE = task.include
                self.RUN_AT_TIME = task.load_run_at_time()
                return task
            except:
                return None

    def in_time_range(self, last_run):
        ERROR_TIME = 180  # Seconds

        dates_list = self.RUN_AT_TIME
        now = timezone.localtime()

        status = False
        for time in dates_list:
            need_run_datetime = timezone.localtime().replace(hour=time.hour, minute=time.minute, second=0)
            need_run_delta = (now - need_run_datetime).total_seconds()
            last_run_delta = (now - last_run).total_seconds()
            if 0 < need_run_delta <= ERROR_TIME < last_run_delta:
                status = True
        return status

    @property
    def is_time(self) -> bool:
        last_run = self.db_task.last_run.replace(tzinfo=timezone.utc)
        now = timezone.now()
        delta = now - last_run
        if delta.total_seconds() > self.RUN_EVERY_MINS * 60:
            return True
        elif self.in_time_range(last_run):
            return True
        else:
            return False

    def start_task(self):
        self.db_task.running_now = True
        self.db_task.save()
        task_event = TaskEvent()
        task_event.task = self.db_task
        task_event.started = timezone.now()
        task_event.save()
        self.task_event = task_event

    def finish_success_task(self):
        self.db_task.running_now = False
        self.db_task.save()
        self.task_event.success = True
        self.task_event.finished = timezone.now()
        self.task_event.save()

    def finish_error_task(self, short_error, trace):
        self.db_task.running_now = False
        self.db_task.save()
        self.task_event.success = False
        self.task_event.finished = timezone.now()
        self.task_event.log = "{}\n" \
                              "*******************\n" \
                              "{}".format(short_error, trace)
        self.task_event.save()
        if self.db_task.alert:
            for consumer in self.db_task.consumers.all():
                consumer.send_text_message(self.task_event.log)
